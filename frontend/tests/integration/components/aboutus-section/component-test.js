import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('aboutus-section', 'Integration | Component | aboutus section', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{aboutus-section}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#aboutus-section}}
      template block text
    {{/aboutus-section}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
