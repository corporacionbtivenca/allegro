import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function() {
    return this.get("session").fetch().catch(function() {});
  },

  model: function(){
    var lol = this.get("session");
    this.set("lol", lol);
    return {};
  },

  actions: {
    signIn: function(model){
      console.log(model.email, model.password);
      this.get('session').open('firebase', {
        provider: 'password',
        email: model.email,
        password: model.password
      }).then(function(data){
        return console.log(data);
      });
    },

    signOut: function(){
      this.get("session").close();
    }
  },


});
