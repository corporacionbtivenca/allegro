import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    return {};
  },

  actions: {
    sendEmail: function(email){
      if(email){
        console.log(email.subject);
        console.log(email.content);
        console.log(email.email);
        console.log(email.phone);
        console.log(email.name);
        Ember.$.post("/email/send", {
          "email": {
              "subject": email.subject,
              "content": email.content,
              "from": email.email,
              "phone": email.phone,
              "name": email.name
          }
        }).then(function(response){
          if(response){
            toastr.success("Correo Enviado con exito");
            return console.log(response);
          }
        });
      }
    }
  }
});
