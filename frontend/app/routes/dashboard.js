import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    return this.store.query('order', {
      orderBy: 'headquarters',
      startAt: this.get("session.currentUser.email")
    });
  }
});
