import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    return this.store.findAll('order');
  },

  actions: {
    createOrder: function(order){
      var order = this.store.createRecord('order', {
        headquarters: order.headquarters,
        names: order.names,
        instructions: order.instructions,
        address: order.address,
        phone: order.phone
      });

      order.save();

      return toastr.info("orden enviada");
    }
  }
});
