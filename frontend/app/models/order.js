import DS from 'ember-data';

export default DS.Model.extend({
  headquarters: DS.attr('string'),
  names: DS.attr('string'),
  instructions: DS.attr('string'),
  address: DS.attr('string'),
  phone: DS.attr('number')
});
