import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('about');
  this.route('register');
  this.route('promotions');
  this.route('delivery');
  this.route('contact');
  this.route('franchisees');
  this.route('places');
  this.route('login');
  this.authenticatedRoute('dashboard');
});

export default Router;
