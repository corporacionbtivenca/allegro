import Ember from 'ember';

export default Ember.Component.extend({
  subjects: ["Formar Parte del equipo de trabajo", "Deseo ser franquiciado", "Información para eventos", "Proveer insumos o servicios", "Sugerencias", "Otros"],

  actions: {
    /*selectSubject: function(email){
      console.log(email);
      this.set("subject", email);
      console.log(this.get("subject"));
    },*/

    sendEmail: function(email){
      this.sendAction("sendEmail", email);

    }
  }
});
