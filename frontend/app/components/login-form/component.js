import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    signIn: function(model){
      this.sendAction('signIn', model);

      this.set('user.email', null);
      this.set('user.password', null);
    }
  }
});
