import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    createOrder: function(order){
      this.sendAction("createOrder", order);

      this.set("order.headquarters", null);
      this.set("order.names", null);
      this.set("order.instructions", null);
      this.set("order.address", null);
      this.set("order.phone", null);

    }
  }
});
