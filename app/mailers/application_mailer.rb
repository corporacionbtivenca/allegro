class ApplicationMailer < ActionMailer::Base
  default from: 'helados@allegrogelato.com'
  layout 'mailer'
end
