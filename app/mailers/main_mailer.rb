require 'mailgun'

class MainMailer < ApplicationMailer
  default from: "no-reply@bitven.org"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.main_mailer.order_notification.subject
  #
  def order_notification
    @greeting = "Hi"

    mail to: "to@example.org"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.main_mailer.contact_notification.subject
  #
  def contact_notification(email)
    @email = email
    @subject = @email["subject"]
    @from = @email["from"]
    @content = @email["content"]

    print("aqui esta email")
    print("\n")
    print(email)
    print("\n")

    mg_client = Mailgun::Client.new 'key-f1ba6c77fa4bd931ae86701d4a647a5f'

    message_params = {
      from: @from,
      to: "lfbu.112@gmail.com",
      subject: @subject,
      text: @content
    }

    mg_client.send_message 'bitven.org', message_params
    #mail( to: "desarrollo@bitven.org", subject: @email["subject"] )
  end
end
