Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount_ember_app :frontend, to: "/"

  post '/email/send' => 'email_sender#sendMail'
  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
